package com.task.myytask.utils

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import com.task.myytask.databinding.ActivityNoInternetBinding


class NoInternetActivity : AppCompatActivity() {
    lateinit var binding: ActivityNoInternetBinding

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        //setContentView(R.layout.activity_no_internet)
        binding = ActivityNoInternetBinding.inflate(layoutInflater)
        setContentView(binding.root)

        binding.backBtn.setOnClickListener {
            onBackPressed()
        }
    }
}