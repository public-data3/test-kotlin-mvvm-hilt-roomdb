package com.task.myytask.rectrofit
import com.task.myytask.model.ProductResponse
import retrofit2.Response
import retrofit2.http.*

interface ApiService {

    @GET("v3/2f06b453-8375-43cf-861a-06e95a951328")
    suspend fun getProduct() : Response<ProductResponse>
}