package com.task.myytask.model

import android.os.Parcelable
import kotlinx.android.parcel.Parcelize

@Parcelize
data class Product(
    val addToCartButtonText: String,
    val badges: List<String>,
    val brand: String,
    val id: String,
    val imageURL: String,
    val isAddToCartEnable: Boolean,
    val isDeliveryOnly: Boolean,
    val isDirectFromSupplier: Boolean,
    val isFindMeEnable: Boolean,
    val isInTrolley: Boolean,
    val isInWishlist: Boolean,
    val price: List<Price>,
    val purchaseTypes: List<PurchaseType>,
    val ratingCount: String,
    val saleUnitPrice: String,
    val title: String,
    val totalReviewCount: Int
):Parcelable