package com.task.myytask.model

import androidx.room.ColumnInfo
import androidx.room.Entity
import androidx.room.PrimaryKey

@Entity(tableName = "productTable")
data class LocalProduct(
    @PrimaryKey(autoGenerate = true)
    @ColumnInfo(name = "temp_id")
    var temp_id: Int = 0,


    @ColumnInfo(name = "id")
    var id: String,
    @ColumnInfo(name = "imageURL")
    val imageURL: String,
    @ColumnInfo(name = "ratingCount")
    val ratingCount: String,
    @ColumnInfo(name = "saleUnitPrice")
    val saleUnitPrice: String,
    @ColumnInfo(name = "title")
    val title: String,

    )

