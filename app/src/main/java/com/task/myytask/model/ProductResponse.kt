package com.task.myytask.model

import android.os.Parcelable
import kotlinx.android.parcel.Parcelize

@Parcelize
data class ProductResponse(
    val products: List<Product>
):Parcelable