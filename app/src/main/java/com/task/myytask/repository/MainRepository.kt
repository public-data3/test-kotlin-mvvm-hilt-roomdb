package com.task.myytask.repository

import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import com.task.myytask.db.ProductDao
import com.task.myytask.model.LocalProduct
import com.task.myytask.model.Product
import com.task.myytask.model.ProductResponse
import com.task.myytask.rectrofit.ApiService
import com.task.myytask.utils.NetworkResult
import com.task.myytask.utils.internetCheck
import kotlinx.coroutines.flow.Flow
import javax.inject.Inject

class MainRepository @Inject constructor(
    private val apiService: ApiService,
    private val productDao: ProductDao
) {

    val productsMutableLiveData = MutableLiveData<NetworkResult<ProductResponse>>()
    val productLiveData: LiveData<NetworkResult<ProductResponse>>
        get() = productsMutableLiveData


    suspend fun getProducts() {

        if (!internetCheck()) {
            productsMutableLiveData.postValue(NetworkResult.Internet("Woops,  Internet connection not available"))

        } else {
            productsMutableLiveData.postValue(NetworkResult.Loading())
            val result = apiService.getProduct()
            productDao.deleteAllRecords()
            if (result.isSuccessful && result.body() != null) {
                productsMutableLiveData.postValue(NetworkResult.Success(result.body()!!))
            } else {
                productsMutableLiveData.postValue(NetworkResult.Error("Something went wrong."))
            }
        }
    }

    fun storeFavProducts(products: Product) {
        var localProduct = LocalProduct(
            0,
            products.id, products.imageURL,
            products.ratingCount, products.saleUnitPrice, products.title
        )
        productDao.insertRecords(localProduct)


    }

    val getAllFabProducrt: Flow<List<LocalProduct>> = productDao.getAllRecords()

    fun removeProduct(id: String) {
        productDao.deleteRecords(id)
    }


}

