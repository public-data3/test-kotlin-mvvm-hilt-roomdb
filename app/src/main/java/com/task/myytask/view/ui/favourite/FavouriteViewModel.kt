package com.task.myytask.view.ui.favourite

import androidx.lifecycle.LiveData
import androidx.lifecycle.ViewModel
import androidx.lifecycle.asLiveData
import androidx.lifecycle.viewModelScope
import com.task.myytask.model.LocalProduct
import com.task.myytask.repository.MainRepository
import dagger.hilt.android.lifecycle.HiltViewModel
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.flow.flowOn
import kotlinx.coroutines.launch
import javax.inject.Inject

@HiltViewModel
class FavouriteViewModel@Inject constructor(val mainRepository: MainRepository)  : ViewModel() {


        val getAllFabProduct: LiveData<List<LocalProduct>> = mainRepository.getAllFabProducrt
        .flowOn(Dispatchers.Main).asLiveData(context = viewModelScope.coroutineContext)


          fun removeFavClicked(id: String) {
              mainRepository.removeProduct(id)

        }

}