package com.task.myytask.view.ui.favourite

import android.os.Bundle
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.Fragment
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProvider
import androidx.recyclerview.widget.LinearLayoutManager
import com.task.myytask.databinding.FragmentDashboardBinding
import com.task.myytask.model.LocalProduct
import com.task.myytask.model.Product
import dagger.hilt.android.AndroidEntryPoint

@AndroidEntryPoint
class FavoriteFragment : Fragment() {

      private var _binding: FragmentDashboardBinding? = null
      private lateinit var favouriteViewModel: FavouriteViewModel
      lateinit var localProductAdapter: LocalProductAdapter

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?
    ): View {
        favouriteViewModel = ViewModelProvider(this).get(FavouriteViewModel::class.java)
         _binding = FragmentDashboardBinding.inflate(inflater, container, false)
        localProductAdapter = LocalProductAdapter(::removeFavourite,::addToCart)
        _binding!!.favproductRecycleView .apply {
                adapter = localProductAdapter
                layoutManager = LinearLayoutManager(requireActivity())
            }
        updateUi()

        val root: View = _binding!!.root


        return root
    }

    private fun updateUi() {


        favouriteViewModel.getAllFabProduct.observe(requireActivity(), Observer { response->

            var list:List<LocalProduct> = response as List<LocalProduct>

            localProductAdapter.submitList(list)

        })


    }


    private fun removeFavourite(id: String){
        favouriteViewModel.removeFavClicked(id)
    }
    private fun addToCart(products: LocalProduct) {

    }

    override fun onDestroyView() {
        super.onDestroyView()
        _binding = null
    }
}