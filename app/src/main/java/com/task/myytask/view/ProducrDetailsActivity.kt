package com.task.myytask.view

import android.os.Bundle
import android.util.Log
import android.view.MenuItem
import androidx.appcompat.app.AppCompatActivity
import com.bumptech.glide.Glide
import com.task.myytask.R
import com.task.myytask.databinding.ActivityProducrDetailsBinding
import com.task.myytask.model.Product

class ProducrDetailsActivity : AppCompatActivity() {
    lateinit var binding: ActivityProducrDetailsBinding

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)


        binding = ActivityProducrDetailsBinding.inflate(layoutInflater)
        setContentView(binding.root)
        binding.lifecycleOwner = this


        setSupportActionBar(binding.toolbarDetails)
        supportActionBar?.setDisplayHomeAsUpEnabled(true)
        supportActionBar?.setDisplayShowHomeEnabled(true)
        supportActionBar?.title = "Product Details"

        val products = intent.getParcelableExtra<Product>("selectedProduct")
        Log.d("TAG", "@@@@@@@@@@@@" + products!!.title)
        binding.model = products
        Glide
            .with(binding.prodImage.context)
            .load(products.imageURL)
            .centerCrop()
            .into(binding.prodImage)
        binding.favBtn.setOnClickListener {
            binding.favBtn.setImageResource(R.drawable.like)
        }

    }


    override fun onOptionsItemSelected(item: MenuItem): Boolean {
        when (item.itemId) {
            android.R.id.home -> onBackPressed()
        }
        return super.onOptionsItemSelected(item)
    }
}