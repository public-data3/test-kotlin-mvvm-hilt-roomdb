package com.task.myytask.view.ui.home
import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import com.task.myytask.model.Product
import com.task.myytask.repository.MainRepository
import dagger.hilt.android.lifecycle.HiltViewModel
import kotlinx.coroutines.launch
import javax.inject.Inject

@HiltViewModel
class HomeViewModel@Inject constructor(val mainRepository: MainRepository): ViewModel() {

    val productLiveData get() = mainRepository.productLiveData


    fun getProduct(){
        viewModelScope.launch {
            mainRepository.getProducts()
        }
    }
    
    
    fun addToFavClicked(products: Product) {

         viewModelScope.launch {
           mainRepository.storeFavProducts(products)
        }


    }


    fun addToCartClicked(products: Product) {

    }

}