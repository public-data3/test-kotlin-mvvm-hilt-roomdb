package com.task.myytask.view.ui.home

import android.annotation.SuppressLint
import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.recyclerview.widget.DiffUtil
import androidx.recyclerview.widget.ListAdapter
import androidx.recyclerview.widget.RecyclerView
import com.bumptech.glide.Glide
import com.task.myytask.R
import com.task.myytask.databinding.ProductListRowBinding
import com.task.myytask.model.Product

class ProductAdapter (
    val addToFavClicked: (Product) -> Unit,
    val addToCartClicked: (Product) -> Unit,
    val  rowClicked: (Product) -> Unit
   ) : ListAdapter<Product, ProductAdapter.ProductViewHolder>(ProductComparator()) {
   @SuppressLint("SuspiciousIndentation")
   override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ProductViewHolder {

      val binding =
            ProductListRowBinding.inflate(LayoutInflater.from(parent.context), parent, false)
        return ProductViewHolder(binding)
   }

   override fun onBindViewHolder(holder: ProductViewHolder, position: Int) {
       val currentItem = getItem(position)
        if (currentItem != null) {
            holder.bind(currentItem)
        }
   }
  inner class ProductViewHolder(private val binding: ProductListRowBinding) :
      RecyclerView.ViewHolder(binding.root) {
      fun bind(productData: Product) {


         binding.apply {

             Glide
            .with(prodImage.context)
            .load(productData.imageURL)
            .centerCrop()
            .into(prodImage)
           prodNameTxt.text =   "Name: "+productData.title

           prodPriceTxt.text = "Unit Price: Rs. "+productData.saleUnitPrice

           favBtn.setOnClickListener {
                 favBtn.setImageResource(R.drawable.like)
                 addToFavClicked(productData)
             }

           addToCart.setOnClickListener {
                 addToCartClicked(productData)
             }
             rowCard.setOnClickListener {
                 rowClicked(productData)
             }

         }
      }
   }

   class ProductComparator : DiffUtil.ItemCallback<Product>() {
      override fun areItemsTheSame(oldItem: Product, newItem: Product) =
         oldItem.title == newItem.title

      override fun areContentsTheSame(oldItem: Product, newItem: Product) =
         oldItem == newItem
   }


}
