package com.task.myytask.view.ui.home

import android.content.Intent
import android.os.Bundle
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.Fragment
import androidx.lifecycle.ViewModelProvider
import androidx.recyclerview.widget.LinearLayoutManager
import com.task.myytask.databinding.FragmentHomeBinding
import com.task.myytask.model.Product
import com.task.myytask.utils.NetworkResult
import com.task.myytask.utils.NoInternetActivity
import com.task.myytask.view.ProducrDetailsActivity
import dagger.hilt.android.AndroidEntryPoint

@AndroidEntryPoint
class HomeFragment : Fragment() {

    private var _binding: FragmentHomeBinding? = null
    private val binding get() = _binding!!
    private lateinit var homeViewModel:HomeViewModel
    lateinit var productAdapter: ProductAdapter

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?): View {
         homeViewModel = ViewModelProvider(this)[HomeViewModel::class.java]
        _binding = FragmentHomeBinding.inflate(inflater, container, false)
        val root: View = binding.root
        CallHomeData()
        setUpRecycleView()

        UpdateIU()
        return root
    }

    private fun setUpRecycleView() {

        productAdapter = ProductAdapter(::addFavourite,::addToCart,::onRecycleViewItemClicked)
         binding.productRecycleView.apply {
                adapter = productAdapter
                layoutManager = LinearLayoutManager(activity)
            }
    }

    private fun CallHomeData() {
        homeViewModel.getProduct()
    }
    private fun UpdateIU() {

          homeViewModel.productLiveData.observe(requireActivity()){

            when (it){

                is NetworkResult.Internet->{
                    val intent = Intent(activity, NoInternetActivity::class.java)
                         startActivity(intent)

                }
                is NetworkResult.Loading ->{
                    binding.loadingBar.visibility  = View.VISIBLE

                }
                is NetworkResult.Success ->{
                   binding.loadingBar.visibility  = View.GONE

                     if(it.data!=null){
                        Log.d("TAG","Data@@@@@@@@@@")
                         productAdapter.submitList(it.data.products)


                    }
                }
                is NetworkResult.Error ->{
                   binding.loadingBar.visibility  = View.GONE
                }

            }

        }

    }

      private fun addFavourite(products: Product){
        homeViewModel.addToFavClicked(products)
    }
    private fun addToCart(products: Product) {
          homeViewModel.addToCartClicked(products)
    }

     private fun onRecycleViewItemClicked(products: Product) {

        val intent = Intent(activity, ProducrDetailsActivity::class.java)
         intent.putExtra("selectedProduct",products)
        startActivity(intent)


    }




    override fun onDestroyView() {
        super.onDestroyView()
        _binding = null
    }
}