package com.task.myytask.view.ui.favourite

import android.annotation.SuppressLint
import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.recyclerview.widget.DiffUtil
import androidx.recyclerview.widget.ListAdapter
import androidx.recyclerview.widget.RecyclerView
import com.bumptech.glide.Glide
import com.task.myytask.R
import com.task.myytask.databinding.FavproductListRowBinding
import com.task.myytask.databinding.ProductListRowBinding
import com.task.myytask.model.LocalProduct
import com.task.myytask.model.Product

class LocalProductAdapter (
    val removeFavClicked: (String) -> Unit,
    val addToCartClicked: (LocalProduct) -> Unit
   ) : ListAdapter<LocalProduct, LocalProductAdapter.ProductViewHolder>(ProductComparator()) {
   @SuppressLint("SuspiciousIndentation")
   override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ProductViewHolder {

      val binding =
            FavproductListRowBinding.inflate(LayoutInflater.from(parent.context), parent, false)
        return ProductViewHolder(binding)
   }

   override fun onBindViewHolder(holder: ProductViewHolder, position: Int) {
       val currentItem = getItem(position)
        if (currentItem != null) {
            holder.bind(currentItem)
        }
   }
  inner class ProductViewHolder(private val binding: FavproductListRowBinding) :
      RecyclerView.ViewHolder(binding.root) {
      fun bind(productData: LocalProduct) {


         binding.apply {

             Glide
            .with(prodImage.context)
            .load(productData.imageURL)
            .centerCrop()
            .into(prodImage)
           prodNameTxt.text =   "Name: "+productData.title

           prodPriceTxt.text = "Unit Price: Rs. "+productData.saleUnitPrice

           favBtn.setOnClickListener {

                 removeFavClicked(productData.id)
             }

           addToCart.setOnClickListener {
                 addToCartClicked(productData)
             }


         }
      }
   }

   class ProductComparator : DiffUtil.ItemCallback<LocalProduct>() {
      override fun areItemsTheSame(oldItem: LocalProduct, newItem: LocalProduct) =
         oldItem.title == newItem.title

      override fun areContentsTheSame(oldItem: LocalProduct, newItem: LocalProduct) =
         oldItem == newItem
   }


}
