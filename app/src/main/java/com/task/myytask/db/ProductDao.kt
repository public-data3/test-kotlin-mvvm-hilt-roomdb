package com.task.myytask.db
import androidx.room.Dao
import androidx.room.Insert
import androidx.room.OnConflictStrategy
import androidx.room.Query
import com.task.myytask.model.LocalProduct
import kotlinx.coroutines.flow.Flow

@Dao
interface ProductDao {

    @Query("SELECT * FROM productTable")
    fun getAllRecords(): Flow<List<LocalProduct>>

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    fun insertRecords(localProduct: LocalProduct)

    @Query("DELETE FROM productTable")
    fun deleteAllRecords()

    @Query("DELETE FROM productTable WHERE id = :idd")
    fun deleteRecords(idd: String)

}